a, b = list(map(int,input().split()))
ans = []

if 1 <= a <= 10000 and 1 <= b <= 200:
    for i in range(a):
        c = list(map(int,input().split()))
        if len(c) == b:
            if 0 > min(c) or max(c) > 2147483647:
                break
            else:
                ans.append(c)
    ans.sort()
    for j in range(0,len(ans)):
        print(' '.join(list(map(str,ans[j]))))