# 題目：https://zerojudge.tw/ShowProblem?problemid=a013

def Change_Rome (Rome):
    if Rome == 'M':
        return 1000
    elif Rome == 'D':
        return 500
    elif Rome == 'C':
        return 100
    elif Rome == 'L':
        return 50
    elif Rome == 'X':
        return 10
    elif Rome == 'V':
        return 5
    elif Rome == 'I':
        return 1

def Change_Ans (ans):
    while True:
        if  ans // 1000 > 0:
            print('M',end = '')
            ans -= 1000
        elif ans // 900 > 0:
            print('CM',end = '')
            ans -= 900
        elif ans // 500 > 0:
            print('D',end = '')
            ans -= 500
        elif ans // 400 > 0:
            print('CD',end = '')
            ans -= 400
        elif ans // 100 > 0:
            print('C',end = '')
            ans -= 100
        elif ans // 90 > 0:
            print('XC',end = '')
            ans -= 90
        elif ans // 50 > 0:
            print('L',end = '')
            ans -= 50
        elif ans // 40 > 0:
            print('XL',end = '')
            ans -= 40
        elif ans // 10 > 0:
            print('X',end = '')
            ans -= 10
        elif ans // 9 > 0:
            print('IX',end = '')
            ans -= 9
        elif ans // 5 > 0:
            print('V',end = '')
            ans -= 5
        elif ans // 4 > 0:
            print('IV',end = '')   
            ans -= 4
        elif ans // 1 > 0:
            print('I',end = '')
            ans -= 1
        else:
            print()
            break

SumA = 0
SumB = 0
Count = 0

while True:
    a = input()
    if a == '#':
        break
    else:
        a = a.split()
        while True:
            if len(a[0]) - 1 > Count :
                if Change_Rome(a[0][Count + 1]) > Change_Rome(a[0][Count]):
                    SumA += Change_Rome(a[0][Count + 1]) - Change_Rome(a[0][Count])
                    Count += 1
                else:
                    SumA += Change_Rome(a[0][Count])
                Count += 1
            elif len(a[0]) > Count :
                SumA += Change_Rome(a[0][Count])
                Count += 1
            else:
                Count = 0
                break
        
        while True:
            if len(a[1]) - 1 > Count :
                if Change_Rome(a[1][Count + 1]) > Change_Rome(a[1][Count]):
                    SumB += Change_Rome(a[1][Count + 1]) - Change_Rome(a[1][Count])
                    Count += 1
                else:
                    SumB += Change_Rome(a[1][Count])
                Count += 1
            elif len(a[1]) > Count :
                SumB += Change_Rome(a[1][Count])
                Count += 1
            else:
                Count = 0
                break

        if SumA > SumB :
            ans = SumA - SumB
            Change_Ans(ans)              
        elif SumA < SumB :
            ans = SumB - SumA
            Change_Ans(ans)
        elif SumA == SumB :
            print('ZERO')

        SumA = 0
        SumB = 0
