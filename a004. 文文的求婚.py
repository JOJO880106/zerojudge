# 題目：https://zerojudge.tw/ShowProblem?problemid=a004

a = []
while True:
    try:
        b = int(input())
        a.append(b)
    except:
        for i in range(len(a)):
            if (a[i] % 4 == 0 and a[i] % 100 != 0) or a[i] % 400 == 0:
                print('閏年')
            else:
                print('平年')
        break

# while True:
#     try:
#         year = int(input())
#         if year % 400 == 0 or (year % 4 == 0 and year % 100 != 0):
#             print("閏年")
#         else:
#             print("平年")
#     except:
#         break