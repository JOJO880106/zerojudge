# 題目：https://zerojudge.tw/ShowProblem?problemid=a020

a = input()
b = ''
ans = 0
j = 0
k = 9

if a[0] == 'A':
    b += '10'
elif a[0] == 'B':
    b += '11'
elif a[0] == 'C':
    b += '12'
elif a[0] == 'D':
    b += '13'
elif a[0] == 'E':
    b += '14'
elif a[0] == 'F':
    b += '15'
elif a[0] == 'G':
    b += '16'
elif a[0] == 'H':
    b += '17'
elif a[0] == 'I':
    b += '34'
elif a[0] == 'J':
    b += '18'
elif a[0] == 'K':
    b += '19'
elif a[0] == 'L':
    b += '20'
elif a[0] == 'M':
    b += '21'
elif a[0] == 'N':
    b += '22'
elif a[0] == 'O':
    b += '35'
elif a[0] == 'P':
    b += '23'
elif a[0] == 'Q':
    b += '24'
elif a[0] == 'R':
    b += '25'
elif a[0] == 'S':
    b += '26'
elif a[0] == 'T':
    b += '27'
elif a[0] == 'U':
    b += '28'
elif a[0] == 'V':
    b += '29'
elif a[0] == 'W':
    b += '32'
elif a[0] == 'X':
    b += '30'
elif a[0] == 'Y':
    b += '31'
elif a[0] == 'Z':
    b += '33'

for i in range(1,len(a)):
    b += a[i]

while True:
    if j <= 10:
        if j == 0:
            ans += int(b[j])
        elif j <= 9:
            ans += (int(b[j]) * k)
            k -= 1
        elif j == 10:
            ans += int(b[j])
        j += 1
    elif j > 10:
        break

if ans % 10 == 0:
    print('real')
else:
    print('fake')