# 題目：https://zerojudge.tw/ShowProblem?problemid=a024

a, b = map(int, input().split())

if a > 0 and b > 0 and a < 2 ** 31 and b < 2 ** 31:
    while True:
        if a > b:
            if a % b == 0:
                print(b)
                break
            else:
                a = a % b
        elif a < b:
            if b % a == 0:
                print(a)
                break
            else:
                b = b % a
        elif a == b:
            print(a)
            break