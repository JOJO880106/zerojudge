a,b = input().split()
ans = 0

# 可被4整除且不為100整除者為閏年。可被400整除為閏年。可被1000整除為閏年
if 1752 < int(a) <= int(b) <= 10000:
    for i in range(int(a),int(b)+1):
        if i % 4 == 0 and i % 100 != 0 or i % 400 == 0 or i % 1000 == 0:
            ans += 1

print(ans)