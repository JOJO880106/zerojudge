# 題目：https://zerojudge.tw/ShowProblem?problemid=a003

# m, d = input().split()

# a = (int(m) * 2 + int(d)) % 3

# if a == 0:
#     print('普通')
# elif a == 1:
#     print('吉')
# elif a == 2:
#     print('大吉')


m, d = map(int, input().split())

a = (m * 2 + d) % 3

if a == 0:
    print('普通')
elif a == 1:
    print('吉')
elif a == 2:
    print('大吉')