# 題目：https://zerojudge.tw/ShowProblem?problemid=a010

# a = int(input())
# b = []
# c = True
# d = 1
# e = ''

# if a > 1 and a <= 100000000:
#     while c:
#         #抓質數
#         for i in range(2, a + 1):
#             if a % i == 0 and a == i:
#                 a = int(a / i)
#                 b.append(i)
#                 c = False
#                 break
#             elif a % i == 0:
#                 a = int(a / i)
#                 b.append(i)
#                 break
#     # 存答案
#     for j in range(len(b) - 1):
#         if b[j] == b[j + 1]:
#             d += 1
#         elif b[j] != b[j + 1] and d > 1:
#             e += str(b[j]) + '^' + str(d) + ' * '
#             d = 1
#         elif b[j] != b[j + 1] and d == 1:
#             e += str(b[j]) + ' * '
#     if d > 1:
#         e += str(b[-1]) + '^' + str(d)
#     else:
#         e += str(b[-1])
# else:
#     exit(0)

# print(e)

a = int(input())

if a > 1 and a <= 100000000:
    num = 2 #質數從2開始
    count = 0
    while True:
        if a == 1:
            break
        elif a % num == 0:
            while True :
                count += 1
                a = a / num
                if a % num != 0: #更換num後做打印
                    if count > 1:
                        if a == 1:
                            print("%d^%d" %(num, count),end = '')
                        else:
                            print("%d^%d * " %(num, count),end = '')
                    else:
                        if a == 1:
                            print("%d" %(num),end = '')
                        else:
                            print("%d * " %(num),end = '')
                    count = 0
                    break        
        else:
            num += 1
else:
    exit(0)