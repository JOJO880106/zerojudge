try:
    while 1:
        a = input()
        a = a[0:a.index('.')+4]
        a = float(a)
        a = '%.3f' % a
        if a[0] == '-':
            if int(a[-1]) >= 5:
                a = float(a[0:a.index('.')+3]) + -0.01
            else:
                a = a[0:a.index('.')+3]
        else:
            if int(a[-1]) >= 5:
                a = float(a[0:a.index('.')+3]) + 0.01
            else:
                a = a[0:a.index('.')+3]
        a = float(a)
        if a == 0:
            print('0.00')
        else:
            print('%.2f' % a)
except:
    pass