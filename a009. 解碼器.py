# 題目：https://zerojudge.tw/ShowProblem?problemid=a009
# ASCII碼範圍：https://www.ni.com/docs/en-US/bundle/labview/page/glang/ascii_codes.html


# k = 7
# c = ''

# a = str(input())

# for i in range(len(a)):
#     b = ord(a[i]) - k # ord()將字串轉ASCII編碼
#     d = chr(b) # chr()將ASCII編碼轉字元
#     c += d

# print(c)

k = 7
a = str(input())

for i in a:
    print(chr(ord(i) - k), end = '')