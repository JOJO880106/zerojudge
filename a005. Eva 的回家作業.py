# 題目：https://zerojudge.tw/ShowProblem?problemid=a005

a = int(input())
f = []
for i in range(a):
    b, c, d, e = map(int, input().split())

    if d - c == c - b:
        f.append((b, c, d, e, int(e + c - b)))
    else:
        f.append((b, c, d, e, int(e * c / b)))

for j in range(a):
    for k in range(5):
        print(f[j][k], end=' ')
    print('')

# n=int(input()) 

# for i in range(n): 
#     a, b, c, d = map(int, input().split()) 
#     if b - a == c - b == d - c:
#         print(a, b, c, d, d + (d - c))
#     else:
#         print(a, b, c, d, d * (d // c))