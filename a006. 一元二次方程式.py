# 題目：https://zerojudge.tw/ShowProblem?problemid=a006

a, b, c = map(int, input().split())

judge = b ** 2 - 4 * a * c

if judge < 0 :
    print('No real root')
    exit(0) #中断某个程序
root1 = int((judge ** 0.5 - b) / (2 * a))
root2 = int((-1 * (judge ** 0.5) - b) / (2 * a))
if judge > 0 :
    if root2 > root1 :
        # print('Two different roots x1=' + str(root2) , ',', 'x2=' + str(root1))
        print(f'Two different roots x1={root2} , x2={root1}')
    else :
        # print('Two different roots x1=' + str(root1) , ',', 'x2=' + str(root2))
        print(f'Two different roots x1={root1} , x2={root2}')
elif judge == 0 :
    print(f'Two same roots x={root1}')

# a, b, c = map(int, input().split())

# judge = b ** 2 - (4 * a * c)

# if judge < 0 :
#     print('No real root')
#     exit(0) #中断某个程序

# root1 = int((judge ** 0.5 - b) / (2 * a))
# root2 = int((-1 * (judge ** 0.5) - b) / (2 * a))

# if root1 == root2 :
#     print(f'Two same roots x={root1}')
# elif judge > 0 :
#     print(f'Two different roots x1={max(root1, root2)} , x2={min(root1, root2)}')