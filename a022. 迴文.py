# 題目：https://zerojudge.tw/ShowProblem?problemid=a022

a = input()
b = 0
c = 0

if len(a) % 2 == 0:
    for i in range(0,int(len(a) / 2)):
        b += ord(a[i])
    for j in range(int(len(a) / 2), len(a)):
        c += ord(a[j])
elif len(a) % 2 != 0:
    for k in range(0,int(len(a) // 2)):
        b += ord(a[k])
    for l in range(int(len(a) // 2 + 1), len(a)):
        c += ord(a[l])
if b == c:
    print('yes')
else:
    print('no')

